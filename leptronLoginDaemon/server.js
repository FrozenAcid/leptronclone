var express = require('express')
var fs = require('fs')
var app = express()
var morgan = require('morgan')


app.use(morgan('dev'))
var cryptico = require('cryptico')

var session_dict = {}

var uniqid = require('uniqid');

app.get('/register', function(req, res) {
	unameTry = req.query.uname 
	psswd = req.query.psswd 
	//checking if there is another user with the same user name
	filename = unameTry + "_psswdCheck.json"
	if(fs.existsSync(filename)) {
		return res.json({registerTry: false})
	}
	else {
		writeOBJ = {}

		writeOBJ['psswd'] = psswd
		fs.writeFileSync(filename, JSON.stringify(writeOBJ))

		return res.json({registerTry: true})
	}
})

app.get('/', function(req, res) {
	return res.json({"serverStatus": true})
})
app.get('/auth_session', function(req, res) {
	session_id = req.query.session_id
	if(session_dict[session_id] != null) {
		console.log("has session")
		return res.json({session: true})
	}
	else {
		return res.json({session: false})
	}
})
app.get('/user_data', function(req, res) {
	session = req.query.session 

	if(!(session in session_dict)) {
		console.log('no session')
		return res.json({sessionValid: false})
	}
	
	var uname = session_dict[session]
	var filename = "user_data/" + uname + "_user.json"
	if(!(fs.existsSync(filename))) {
		console.log('no filename');
		return res.json({udataempy: true})
	}

	finished_file = fs.readFileSync(filename)
	finishedFileObj = JSON.parse(finished_file)

	return res.json(finishedFileObj)
})
app.get('/add_user_data', function(req, res) {
	stock = req.query.stock
	u_sess_id = req.query.sess_id 
	uname = session_dict[u_sess_id]

	console.log(uname)

	filename = "user_data/" + uname + "_user.json"
	if(!(fs.existsSync(filename))) {
		console.log('no filename');
		var n_json = {}
		n_json['stocks'] = []
		n_json.stocks.push(stock)
		fs.writeFileSync(filename, JSON.stringify(n_json))
	}
	else {
		unfinished_file = fs.readFileSync(filename)
		unfinished_file_obj = JSON.parse(unfinished_file)
		if(unfinished_file_obj.stocks.indexOf(stock) >= 0) {
			console.log('has stock')
			return res.json({stockDuplicated: true})
		}
		else {
			unfinished_file_obj.stocks.push(stock)
			fs.writeFileSync(filename, JSON.stringify(unfinished_file_obj))
			return res.json({added: true})
		}
	}
})
app.get('/login_try', function(req, res) {
	uname = req.query.uname
	psswd = req.query.psswd

	filename = uname + "_psswdCheck.json"
	if(fs.existsSync(filename)) {
		console.log("Has txt file")
		var userData = fs.readFileSync(filename)
		userDataO = JSON.parse(userData)
		//psswd check
		if(psswd == userDataO.psswd) {
			console.log("password matches")

			//generating a session id
			var session_uniq_id = uniqid()
			console.log(session_uniq_id)
			if(session_dict[uname]) {
				console.log("already has key")
				return res.json({userLoginAuth: true, session_id: session_uniq_id})
			}
			session_dict[session_uniq_id] = uname

			return res.json({userLoginAuth: true, session_id: session_uniq_id})
		}
		else {
			return res.json({userPasswordAuth: false})
		}
	}
	else {
		console.log("No text file ")
		return res.json({userStatusFound: false})
	}

	return res.send("recieved")
})


app.listen(80)
