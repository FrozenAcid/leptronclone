var express = require('express');
var app = express();
var path = require('path');
var fs = require('fs');
const https = require('https');
const http = require('http')
var handlebars = require('handlebars');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var Mustache = require('mustache');
var session = require('express-session');
var morgan = require('morgan');

app.use('/js', express.static(path.join(__dirname, 'js')))
app.use('/css', express.static(path.join(__dirname, 'css')))
app.use('/fonts', express.static(path.join(__dirname, 'fonts')))
app.use(morgan('dev'));

/*
  * Generate the General Keys Here
*/


var JSEncrypt = require('node-jsencrypt')
var cryptico = require('cryptico')
var loginDaemonUrl = "http://ec2-18-222-170-55.us-east-2.compute.amazonaws.com"

app.get('/', function(req, res) {
	return res.sendFile(path.join(__dirname + "/index.html"))
})
app.get('/signup', function(req, res) {
	return res.sendFile(path.join(__dirname + "/signup.html"))
})

app.get('/api/spq', function(req, res) {
	var spqLst = ['MSFT', 'YHOO', 'FB', 'AAPL', 'GOOGL', 'KKR', 'BA', 'M', 'VZ', 'AXP', 'AMZN', 'PKG', 'WMT', 'XOM', 'BRK', 'T', 'GM', 'F', 'CVX', 'COST', 'KR', 'GE']
	var retLST = []
	for(var i = 0; i != 10; i++) {
		mLen = spqLst.length + 1
		var num = Math.floor(Math.random() * mLen)
		var ticker = spqLst[num]
		retLST.push(ticker)
	}
	return res.json({tickerList: retLST})
})
app.get('/api/batch/:one/:two/:three/:four/:five/:six/:seven/:eight/:nine/:ten', function(req, res) {
	var one = req.params.one
	var two = req.params.two
	var three = req.params.three
	var four = req.params.four
	var five = req.params.five
	var six = req.params.six
	var seven = req.params.seven
	var eight = req.params.eight
	var nine = req.params.nine
	var ten = req.params.ten

	var retLst = []
	var batch_url = "https://api.iextrading.com/1.0/stock/market/batch?symbols=" + one + "," + two + "," + three + "," + four + "," + five + "," + six + "," + seven + "," + eight + "," + nine + "," + ten + "&types=quote"
	https.get(batch_url, (resp) => {
		let data = ''
		resp.on('data', (chunk) => {
			data += chunk
		})
		resp.on('end', () => {
			var jsonQuote = JSON.parse(data)
			return res.json(jsonQuote)
		})
	})
})
app.get('/api/batch/:one', function(req, res) {
	var one = req.params.one

	var retLst = []
	var batch_url = "https://api.iextrading.com/1.0/stock/market/batch?symbols=" + one + "&types=quote"
	https.get(batch_url, (resp) => {
		let data = ''
		resp.on('data', (chunk) => {
			data += chunk
		})
		resp.on('end', () => {
			var jsonQuote = JSON.parse(data)
			return res.json(jsonQuote)
		})
	})
})

//next url https://api.iextrading.com/1.0/stock/market/list/gainers
app.get('/api/winner', function(req, res) {
	var url = "https://api.iextrading.com/1.0/stock/market/list/gainers"
	https.get(url, (resp) => {
		let data = ''
		resp.on('data', (chunk) => {
			data += chunk
		})
		resp.on('end', () => {
			var jsonQuote = JSON.parse(data)
			return res.json(jsonQuote)
		})
	})
})

app.get('/stock/:ticker', function(req, res) {
	try {
		var ticker = req.params.ticker
		var url = "https://api.iextrading.com/1.0/stock/" + ticker + "/batch?types=quote,news,chart,financials&range=30m&last=10"
		https.get(url, (resp) => {
			let data = ''
			resp.on('data', (chunk) => {
				data += chunk
			})
			resp.on('end', () => {
				var quote = JSON.parse(data)
				fs.readFile('stock.html', 'utf-8', function(err, data) {
					if(err) {
						console.log(err)
					}
					var newsOBJ = quote.news
					var newsLST = []
					for(var i = 0; i != newsOBJ.length; i++) {
						newsLST.push({title: newsOBJ[i].headline, source: newsOBJ[i].source})
					}
					const iexFinanceApiUrl = "https://api.iextrading.com/1.0/stock/" + ticker + "/financials"
					https.get(iexFinanceApiUrl, (iexResp) => {
						let iexData = ''
						iexResp.on('data', (iexChunk) => {
							iexData += iexChunk
						})
						iexResp.on('end', () => {
							var financeJson = JSON.parse(iexData)
							try {
								var financeOne = {
									date: financeJson.financials[0].reportDate,
									totalRevenue: financeJson.financials[0].totalRevenue,
									costOfRevenue: financeJson.financials[0].costOfRevenue,
									grossProfit: financeJson.financials[0].grossProfit,
									netIncome: financeJson.financials[0].netIncome,
									operatingRevenue: financeJson.financials[0].operatingRevenue,
									researchAndDevelopment: financeJson.financials[0].researchAndDevelopment,
		                            operatingExpense: financeJson.financials[0].operatingExpense,
		                            currentAssets: financeJson.financials[0].currentAssets,
		                            totalAssets: financeJson.financials[0].totalAssets,
		                            currentCash: financeJson.financials[0].currentCash,
		                            currentDebt: financeJson.financials[0].currentDebt,
		                            cashFlow: financeJson.financials[0].cashFlow
								}
							} catch(err) {
								return res.send("failed 404: return to home <a href='/'>HOME</a>")
							}
							var financeTwo = {
								date: financeJson.financials[1].reportDate,
								totalRevenue: financeJson.financials[1].totalRevenue,
								costOfRevenue: financeJson.financials[1].costOfRevenue,
								grossProfit: financeJson.financials[1].grossProfit,
								netIncome: financeJson.financials[1].netIncome,
								operatingRevenue: financeJson.financials[1].operatingRevenue,
								researchAndDevelopment: financeJson.financials[1].researchAndDevelopment,
	                            operatingExpense: financeJson.financials[1].operatingExpense,
	                            currentAssets: financeJson.financials[1].currentAssets,
	                            totalAssets: financeJson.financials[1].totalAssets,
	                            currentCash: financeJson.financials[1].currentCash,
	                            currentDebt: financeJson.financials[1].currentDebt,
	                            cashFlow: financeJson.financials[1].cashFlow
							}
							var financeThree = {
								date: financeJson.financials[2].reportDate,
								totalRevenue: financeJson.financials[2].totalRevenue,
								costOfRevenue: financeJson.financials[2].costOfRevenue,
								grossProfit: financeJson.financials[2].grossProfit,
								netIncome: financeJson.financials[2].netIncome,
								operatingRevenue: financeJson.financials[2].operatingRevenue,
								researchAndDevelopment: financeJson.financials[2].researchAndDevelopment,
	                            operatingExpense: financeJson.financials[2].operatingExpense,
	                            currentAssets: financeJson.financials[2].currentAssets,
	                            totalAssets: financeJson.financials[2].totalAssets,
	                            currentCash: financeJson.financials[2].currentCash,
	                            currentDebt: financeJson.financials[2].currentDebt,
	                            cashFlow: financeJson.financials[2].cashFlow
							}
							var financeFour = {
								date: financeJson.financials[3].reportDate,
								totalRevenue: financeJson.financials[3].totalRevenue,
								costOfRevenue: financeJson.financials[3].costOfRevenue,
								grossProfit: financeJson.financials[3].grossProfit,
								netIncome: financeJson.financials[3].netIncome,
								operatingRevenue: financeJson.financials[3].operatingRevenue,
								researchAndDevelopment: financeJson.financials[3].researchAndDevelopment,
	                            operatingExpense: financeJson.financials[3].operatingExpense,
	                            currentAssets: financeJson.financials[3].currentAssets,
	                            totalAssets: financeJson.financials[3].totalAssets,
	                            currentCash: financeJson.financials[3].currentCash,
	                            currentDebt: financeJson.financials[3].currentDebt,
	                            cashFlow: financeJson.financials[3].cashFlow
							}
							const objData = {
								ticker: ticker,
								companyName: quote.quote.companyName,
								price: quote.quote.latestPrice,
								volume: quote.quote.latestVolume,
								prevClose: quote.quote.previousClose,
								change: quote.quote.change,
								peRatio: quote.quote.peRatio,
								news: newsLST,
								finance_1: financeOne,
								finance_2: financeTwo,
								finance_3: financeThree,
								finance_4: financeFour
							}
							const template = handlebars.compile(data, {strict: true})
							const result = template(objData)
							return res.send(result)
						})
					})
				})
			})
		})
	}
	catch(error) {
		return res.send("404 error")
	}
})
app.get('/api/graph/:ticker', function(req, res) {
	var ticker = req.params.ticker
	var url = "https://api.iextrading.com/1.0/stock/" + ticker + "/chart"
	https.get(url, (resp) => {
		let data = ''
		resp.on('data', (chunk) => {
			data += chunk
		})
		resp.on('end', () => {
			var quoteJSON = JSON.parse(data)
			var labelLST = []
			var priceLST = []
			var financeUri = "https://api.iextrading.com/1.0/stock/" + ticker + "/financials"
			for(var i = 0; i != quoteJSON.length; i++) {
				labelLST.push(quoteJSON[i].date)
				priceLST.push(quoteJSON[i].close)
			}
			https.get(financeUri, (financeResp) => {
				let fin_data = ''
				financeResp.on('data', (finChunk) => {
					fin_data += finChunk
				})

				financeResp.on('end', () => {
					financeQuote = JSON.parse(fin_data)

					var finance_labels = []
					var finance_price = []

					for(var i = 0; i != financeQuote.financials.length; i++) {
						finance_labels.push(financeQuote.financials[i].reportDate)
						finance_price.push(financeQuote.financials[i].totalRevenue)
					}

					return res.json({price: priceLST, labels: labelLST, financeChart: {financeLabel: finance_labels, financePrice: finance_price}})
				})
			})
		})
	})
})
var chart_template = "<script>! function (e) {\r\n\t\t\t\n\t\t\tvar t = function () {\r\n\t\t\t\tthis.$body = e(\"body\"), this.charts = []\r\n\t\t\t};\r\n\t\t\tt.prototype.respChart = function (t, r, a, o) {\r\n\t\t\t\tvar n = Chart.controllers.bar.prototype.draw;\r\n\t\t\t\tChart.controllers.bar = Chart.controllers.bar.extend({\r\n\t\t\t\t\tdraw: function () {\r\n\t\t\t\t\t\tn.apply(this, arguments);\r\n\t\t\t\t\t\tvar e = this.chart.chart.ctx,\r\n\t\t\t\t\t\t\tt = e.fill;\r\n\t\t\t\t\t\te.fill = function () {\r\n\t\t\t\t\t\t\te.save(), e.shadowColor = \"rgba(0,0,0,0.01)\", e.shadowBlur = 20, e.shadowOffsetX = 4, e.shadowOffsetY = 5, t.apply(this, arguments), \
 e.restore()\r\n\t\t\t\t\t\t}\r\n\t\t\t\t\t}\r\n\t\t\t\t});\r\n\t\t\t\tvar i = t.get(0).getContext(\"2d\"),\r\n\t\t\t\t\ts = e(t).parent();\r\n\t\t\t\treturn function () {\r\n\t\t\t\t\tvar n; \
     \r\n\t\t\t\t\tswitch (t.attr(\"width\", e(s).width()), r) {\r\n\t\t\t\t\t\tcase \"Line\":\r\n\t\t\t\t\t\t\tn = new Chart(i, {\r\n\t\t\t\t\t\t\t\ttype: \"line\",\r\n\t\t\t\t\t\t\t\tdata: a,\r\n\t\t\t\t\t\t\t\toptions: o\r\n\t\t\t\t\t\t\t});\r\n\t\t\t\t\t\t\tbreak; \ \r\n\t\t\t\t\t\tcase \"Doughnut\":\r\n\t\t\t\t\t\t\tn = new Chart(i, {\r\n\t\t\t\t\t\t\t\ttype: \"doughnut\",\r\n\t\t\t\t\t\t\t\tdata: a,\r\n\t\t\t\t\t\t\t\toptions: o\r\n\t\t\t\t\t\t\t});\r\n\t\t\t\t\t\t\tbreak;\r\n\t\t\t\t\t\tcase \"Pie\":\r\n\t\t\t\t\t\t\tn = new Chart(i, {\r\n\t\t\t\t\t\t\t\ttype: \"pie\",\r\n\t\t\t\t\t\t\t\tdata: a,\r\n\t\t\t\t\t\t\t\toptions: o\r\n\t\t\t\t\t\t\t});\r\n\t\t\t\t\t\t\tbreak;\r\n\t\t\t\t\t\tcase \"Bar\":\r\n\t\t\t\t\t\t\tn = new Chart(i, {\r\n\t\t\t\t\t\t\t\ttype: \"bar\",\r\n\t\t\t\t\t\t\t\tdata: a,\r\n\t\t\t\t\t\t\t\toptions: o\r\n\t\t\t\t\t\t\t});\r\n\t\t\t\t\t\t\tbreak;\r\n\t\t\t\t\t\tcase \"Radar\":\r\n\t\t\t\t\t\t\tn = new Chart(i, {\r\n\t\t\t\t\t\t\t\ttype: \"radar\",\r\n\t\t\t\t\t\t\t\tdata: a,\r\n\t\t\t\t\t\t\t\toptions: o\r\n\t\t\t\t\t\t\t});\r\n\t\t\t\t\t\t\tbreak;\r\n\t\t\t\t\t\tcase \"PolarArea\":\r\n\t\t\t\t\t\t\tn = new Chart(i, {\r\n\t\t\t\t\t\t\t\tdata: a,\r\n\t\t\t\t\t\t\t\ttype: \"polarArea\",\r\n\t\t\t\t\t\t\t\toptions: o\r\n\t\t\t\t\t\t\t})\r\n\t\t\t\t\t}\r\n\t\t\t\t\treturn n\r\n\t\t\t\t}()\r\n\t\t\t}, \
      t.prototype.initCharts = function () {\r\n\t\t\t\tif (e(\"#{{chartId}}\").length > 0) {\r\n\t\t\t\t\tvar t = document.getElementById(\"{{chartId}}\").getContext(\"2d\").createLinearGradient(0, 500, 0, 150);\r\n\t\t\t\t\tt.addColorStop(.85, \"#38ef7d\"), t.addColorStop(1, \"#11998e\");\r\n\t\t\t\t\tvar r = {\r\n\t\t\t\t\t\tlabels: {{{chartLabels}}},\r\n\t\t\t\t\t\tdatasets: [{\r\n\t\t\t\t\t\t\tlabel: \"{{chartLabelName}}\",\r\n\t\t\t\t\t\t\tbackgroundColor: t,\r\n\t\t\t\t\t\t\tborderColor: t,\r\n\t\t\t\t\t\t\thoverBackgroundColor: t,\r\n\t\t\t\t\t\t\thoverBorderColor: t,\r\n\t\t\t\t\t\t\tdata: {{chartData}}\r\n\t\t\t\t\t\t}]\r\n\t\t\t\t\t};\r\n\t\t\t\t\t[].push(this.respChart(e(\"#{{chartId}}\"), \"{{chartType}}\", r, {\r\n\t\t\t\t\t\tmaintainAspectRatio: !1,\r\n\t\t\t\t\t\tlegend: {\r\n\t\t\t\t\t\t\tdisplay: !1\r\n\t\t\t\t\t\t},\r\n\t\t\t\t\t\tscales: {\r\n\t\t\t\t\t\t\tyAxes: [{\r\n\t\t\t\t\t\t\t\tgridLines: {\r\n\t\t\t\t\t\t\t\t\tdisplay: !1\r\n\t\t\t\t\t\t\t\t},\r\n\t\t\t\t\t\t\t\tstacked: !1,\r\n\t\t\t\t\t\t\t\tticks: {\r\n\t\t\t\t\t\t\t\t\tstepSize: 20\r\n\t\t\t\t\t\t\t\t}\r\n\t\t\t\t\t\t\t}],\r\n\t\t\t\t\t\t\txAxes: [{\r\n\t\t\t\t\t\t\t\tbarPercentage: .7,\r\n\t\t\t\t\t\t\t\tcategoryPercentage: .5,\r\n\t\t\t\t\t\t\t\tstacked: !1,\r\n\t\t\t\t\t\t\t\tgridLines: {\r\n\t\t\t\t\t\t\t\t\tcolor: \"rgba(0,0,0,0.01)\"\r\n\t\t\t\t\t\t\t\t}\r\n\t\t\t\t\t\t\t}]\r\n\t\t\t\t\t\t}\r\n\t\t\t\t\t}))\r\n\t\t\t\t}\r\n\t\t\t}, t.prototype.init = function () {\r\n\t\t\t\tvar t = this;\r\n\t\t\t\tChart.defaults.global.defaultFontFamily = \'-apple-system,BlinkMacSystemFont,\"Segoe UI\",Roboto,Oxygen-Sans,Ubuntu,Cantarell,\"Helvetica Neue\",sans-serif\', t.charts = this.initCharts(), e(window).on(\"resize\", function (r) {\r\n\t\t\t\t\te.each(t.charts, function (e, t) {\r\n\t\t\t\t\t\ttry {\r\n\t\t\t\t\t\t\tt.destroy()\r\n\t\t\t\t\t\t} catch (e) { }\r\n\t\t\t\t\t}), t.charts = t.initCharts()\r\n\t\t\t\t})\r\n\t\t\t}, e.Profile = new t, e.Profile.Constructor = t\r\n\t\t}(window.jQuery),\r\n\t\t\tfunction (e) {\r\n\t\t\t\t\"use strict\";\r\n\t\t\t\te.Profile.init()\r\n\t\t\t}(window.jQuery);</script>"
app.get('/chart/:ticker/:chartId', function(req , res) {
	try {
		var chartId = req.params.chartId
		var ticker = req.params.ticker
		var url = "https://api.iextrading.com/1.0/stock/" + ticker + "/chart"

		try {
				https.get(url, (resp) => {
				let data = ''
				resp.on('data', (chunk) => {
		            data += chunk
		        })
		        resp.on('end', () => {
		        	if (data == undefined) {
		        		console.log("RETARD")
		        		return res.send("FAILED")
		        	}
		        	try {
		        		var chartTokens = JSON.parse(data)
		        	}
		        	catch(err) {
		        		return res.send("failed")
		        	}

		                var dataStr = "["
		                var labelStr = "["
		                for (var x = 0; x != chartTokens.length; x++) {
		                    var chartToken = chartTokens[x]
		                    dataStr += chartToken.close + ","
		                    var labelToken = chartToken.date
		                    labelStr += '"'
		                    labelStr += labelToken
		                    labelStr += '"'
		                    labelStr += ','
		                }
		                dataStr = dataStr.substring(0, dataStr.length - 1)
		                labelStr = labelStr.substring(0, labelStr.length - 1)
		                dataStr += "]"
		                labelStr += "]"
		            var handlebarsData = {
		            	chartId: chartId,
		            	chartLabels: labelStr,
		            	chartLabelName: ticker + " - Stock Price",
		            	chartData: dataStr,
		            	chartType: "Line"
		            }
		            var retHtml = Mustache.to_html(chart_template, handlebarsData)
		            return res.send(retHtml)
		        })
			})
		}
		catch (err) {
			console.error("err");
		}
	}
	catch (error) {
		console.log("FAILED")
	}
})

app.get('/news/rss/:ticker', function(req, res) {
	const ticker = req.params.ticker;
	const url = "https://feeds.finance.yahoo.com/rss/2.0/headline?s=" + ticker;
	https.get(url, (resp) => {
		let data = '';
		resp.on('data', (chunk) => {
			data += chunk
		})
		resp.on('end', () => {
			var parseString = require('xml2js').parseString;
			var xml = data
			parseString(xml, function(err, result) {
				const fin = result.rss.channel[0]
				var retLst = []
				for(var i = 0; i != fin.item.length; i++) {
					var item = fin.item[i]
					var appJson = {
						title: item.title[0],
						link: item.link[0],
						description: item.description[0],
						pubDate: item.pubDate[0]
					}
					retLst.push(appJson)
				}
				return res.send(retLst)
			})
		})
	})
})

app.get("/peers/:ticker", function(req, res) {
	var request = require('sync-request')
	var ticker = req.params.ticker
	var url = "https://api.iextrading.com/1.0/stock/" + ticker + "/peers"
	https.get(url, (resp) => {
		let data = '';
		resp.on('data', (chunk) => {
			data += chunk
		})
		resp.on('end', () => {
			var peers = JSON.parse(data)
			var peerLst = []
			fin_url = "https://api.iextrading.com/1.0/stock/market/batch?symbols="
			for(var i = 0; i != peers.length; i++) {
				var peer = peers[i]
				fin_url += peer
				if(i != peers.length) {
					fin_url += ","
				}
			}
			fin_url.substring(0, fin_url.length - 1)
			fin_url += "&types=quote"
			https.get(fin_url, (batch_resp) => {
				let batch_data = ''
				batch_resp.on('data', (batch_chunk) => {
					batch_data += batch_chunk
				})
				batch_resp.on('end', () => {
					batchJson = JSON.parse(batch_data)
					for(var i = 0; i!= peers.length; i++) {
						var peer = peers[i]
						var peerQuote = batchJson[peer]["quote"]
						var appendJson = {
							ticker: peerQuote.symbol,
							fullName: peerQuote.companyName,
							changeUSD: peerQuote.change,
							url: "/stock/" + peerQuote.symbol,
							priceUsd: peerQuote.latestPrice
						}
						peerLst.push(appendJson)
					}
					return res.json(peerLst)
				})
			})
		})
	})
})

var privateKEY = fs.readFileSync('./keys/private.key');
var publicKEY = fs.readFileSync('./keys/public.key')
const jwt  = require('jsonwebtoken');

const i = "Leptron"
const s = "leptronUser"
const a = "leptronDev"

var signingOptions = {
	issuer: i,
	subject: s,
	audience: a,
	expiresIn: "12h",
	algorithm: "RS256"
}

app.get('/getKey', function(req, res) {
	return res.send(privateKEY)
})

app.get("/login_try", function(req, res) {
	var payload = req.query.payload

	var check = jwt.verify(payload, publicKEY, {algorithm: "RS256"})

	http.get(loginDaemonUrl + "/login_try?psswd=" + check.psswd + "&uname=" + check.uname, (resp) => {
		let data = ''
		resp.on('data', (chunk) => {
			data += chunk
		})
		resp.on('end', () => {
			if(data == "recieved") {
				console.log('gotcha')
			}
			if(data != "recieved") {
				dataObject = JSON.parse(data)
				if(dataObject.userStatusFound == false) {
					return res.json({usernameFound: false})
				}
				if(dataObject.userPasswordAuth == false) {
					return res.json({passwordAuth: false})
				}
				res.json(dataObject)
			}
		})
	})
	return true
})
app.get("/auth_session", function(req, res) {

	session_id = req.query.session_id

	http.get(loginDaemonUrl + "/auth_session?session_id=" + session_id, (resp) => {
		let data = ''
		resp.on('data', (chunk) => {
			data += chunk
		})
		resp.on('end', () => {
			var obj = JSON.parse(data)
			if(obj.session == true) {
				return res.json({sessionAuth: true})
			}
			if(obj.session == false) {
				return res.json({sessionAuth: false})
			}
		})
	})
})
app.get('/getUserData', function(req, res) {
	session = req.query.session 
	http.get(loginDaemonUrl + "/user_data?session=" + session, (resp) => {
		let data = ''
		resp.on('data', (chunk) => {
			data += chunk
		})
		resp.on('end', () => {
			console.log(data)
			finOBJ = JSON.parse(data)
			return res.json(finOBJ)
		})
	})
})

app.get('/signupJSON', function(req, res) {
	uname = req.query.uname 
	psswd = req.query.psswd 
	http.get(loginDaemonUrl + "/register?uname=" + uname + "&psswd=" + psswd, (resp) => {
		let data = ''
		resp.on('data', (chunk) => {
			data += chunk
		})
		resp.on('end', () => {
			return res.send(data)
		})
	})
})

app.get('/add_user_data', function(req, res) {
	stock = req.query.stock
	u_sess_id = req.query.sess_id 
	
	http.get(loginDaemonUrl + "/add_user_data?stock=" + stock + "&sess_id=" + u_sess_id, (resp) => {
		let data = ''
		resp.on('data', (chunk) => {
			data += chunk
		})
		resp.on('end', () => {
			return res.json(data)
		})
	})
})

var port = process.env.PORT || 8080;
app.listen(port)
